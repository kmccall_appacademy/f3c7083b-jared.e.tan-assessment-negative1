# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  arr = []
  (nums.min..nums.max).each do |i|
    if nums.include?(i) == false
      arr << i
    end
  end
  arr
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  chars = binary.to_s.chars
  start_exponent = chars.length - 1
  num = 0
  chars.each_with_index do |i, idx|
    num += (i.to_i * 2**(start_exponent - idx))
  end
  num
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    new_hash = Hash.new(0)
    self.keys.each do |i|
      new_hash[i] = self[i] if yield(i, self[i]) == true
    end
    new_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.


  #Pretty sure this is correct! I think the 'each argument to the block'
  #spec is missing keys = 0 to find the resulting hash. 
  def my_merge(hash, &prc)
    unless block_given?
      hash.keys.each do |i|
        self[i] = hash[i]
      end
      return self
    end
    new_hash = Hash.new(0)
    self.keys.each do |i|
      hash[i] = 0 unless hash.keys.include?(i)
    end
    hash.keys.each do |i|
      self[i] = 0 unless self.keys.include?(i)
    end
    hash.keys.each do |i|
      new_hash[i] = yield(i, self[i], hash[i])
    end
    new_hash
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  arr = [2, 1]
  if n == 0
    return 2
  elsif n > 0
    idx = 2
    until idx > n
      arr[idx] = arr[idx - 2] + arr[idx - 1]
      idx += 1
    end
  elsif n < 0
    idx = -1
    until idx < n
      arr.unshift(arr[1] - arr[0])
      idx -= 1
    end
    return arr[0]
  end
  arr[n]
end



# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  hash = Hash.new
  (0..string.length).each do |i|
    (i..string.length).each do |i2|
      if string[i, i2] == string[i, i2].reverse
        hash[string[i, i2]] = string[i, i2].length
      end
    end
  end
  return false if hash.values.none? { |i3| i3 > 2 }
  max_value = hash.values.max
  #if the palindrome is wanted :
  #max_hash = hash.select { |_, v| v == max_value }
  #max_hash.keys.sort[0]
end
